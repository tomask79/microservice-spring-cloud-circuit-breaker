# Writing MicroServices [part 2] #

## Using the Circuit-Breaker with Spring Cloud MicroServices ##

When writing MicroServices, you need to tell the MicroService invocation method what to do when particular MicroService is not accessible. There are plenty options what to do when MicroService is not available:

* Call another backup MicroService.
* Return some cached result.
* Return not available page...

Widely used pattern for achieving this is a [Circuit breaker](http://martinfowler.com/bliki/CircuitBreaker.html) pattern. Definitely read this description defined by Martin Fowler before you'll continue reading.

Anyway, in a nutshell. What circuit breaker does is that it wraps your MicroService invocation method in a proxy monitoring failures of MicroService invocation. If failures will reach certain threshold value all of the other calls will end with exception or if you define that with backup plan invocation...Spring Cloud has excellent implementation of this called [Hystrix](https://github.com/Netflix/Hystrix/wiki/How-it-Works). 

## Using the Hystrix circuit breaker tolerance library ##

All right, let's make a little more advance demo than you can find on the internet. Let's take my [first MicroService demo](https://bitbucket.org/tomask79/microservices-spring-cloud) from previous repository with personsService MicroService and add tolerance logic to the invoker with the following rules:

Every 20 seconds (**metrics.rollingStats.timeInMilliseconds**) collect statistics from 6 requests (**circuitBreaker.requestVolumeThreshold**) and if all of them ended with crash (**circuitBreaker.errorThresholdPercentage**) then redirect invocation to the fallback logic. Every 5 seconds try whether MicroService is again available (**circuitBreaker.sleepWindowInMilliseconds**).

Mentioned MicroService invoking component will look like:


```
@Component
public class MicroServiceInvoker {

    @Autowired
    private LoadBalancerClient loadBalancer;

    @HystrixCommand(fallbackMethod = "invokeMicroServiceFallback",
                commandProperties = {
                    @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "100"),
                    @HystrixProperty(name = "metrics.rollingStats.timeInMilliseconds", value = "20000"),
                    @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "6"),
                    @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000")
                }
            )
    public void invokeMicroService() {
        final RestTemplate restTemplate = new RestTemplate();

        final ServiceInstance serviceInstance = loadBalancer.choose("personsService");
        if (serviceInstance != null) {
            System.out.println("Invoking instance at URL: "+serviceInstance.getUri());
            System.out.println("Result :"+
                    restTemplate.getForObject(serviceInstance.getUri()+"/persons",
                            String.class));
        } else {
            System.out.println("Service is down...");
            throw new IllegalStateException("PersonsService is not running!");
        }
    }

    public void invokeMicroServiceFallback() {
        System.out.println("Waiting for circuit-breaker to close again...");
    }
}
```

## Testing the circuit breaker ##

Run the discovery server

```
mvn clean install (in the spring-microservice-registry directory with pom.xml)
java -jar target/demo-0.0.1-SNAPSHOT.war
```

Run the MicroService

```
mvn clean install (in the spring-microservice-service directory with pom.xml)
java -jar target/demo-0.0.1-SNAPSHOT.war
verify with http://localhost:9761 that MicroService is registered.
```

Run the MicroService client

```
mvn clean install (in the spring-microservice-client directory with pom.xml)
java -jar target/demo-0.0.1-SNAPSHOT.war
```

After starting the client you should see output like:

```
.
.
Invocation number :16
Invoking instance at URL: http://192.168.1.112:8080
Result :{"persons":[{"name":"Tomas","surname":"Kloucek","department":"Programmer"}]}
Invocation number :17
Invoking instance at URL: http://192.168.1.112:8080
Result :{"persons":[{"name":"Tomas","surname":"Kloucek","department":"Programmer"}]}
Invocation number :18
Invoking instance at URL: http://192.168.1.112:8080
Result :{"persons":[{"name":"Tomas","surname":"Kloucek","department":"Programmer"}]}
Invocation number :19
.
.
```

Okay, now **turn off the personsService MicroService**...
**20 seconds** you're going to see the output:


```
.
.
Invocation number :18
Invoking instance at URL: http://192.168.1.112:8080
Waiting for circuit-breaker to close again...
Invocation number :19
Invoking instance at URL: http://192.168.1.112:8080
Waiting for circuit-breaker to close again...
.
.
```

then after that just:

```
.
.
Invocation number :78
Waiting for circuit-breaker to close again...
Invocation number :79
Waiting for circuit-breaker to close again...
.
.
```

with every **5** seconds changed to:

```
Invoking instance at URL: http://192.168.1.112:8080
Waiting for circuit-breaker to close again...
```
when Hystrix tests whether MicroService is running again. After you will turn on the MicroService back, circuit should close and your MicroService client will be getting the results as in the beginning...To summarize things, circuit breaker can be found in the following states:

* **OPEN** MicroService call ends up with exception or fallback logic
* **CLOSED** No errors. MicroService is properly called.
* **HALF-OPENED** When circuitBreakerSleepWindowInMilliseconds time occurres the Hystrix will allow the request to invoke the MicroService to test whether it's alive = Half-Opened state.

Again, I showed you necesary steps to make it work. If you want more detailed info then definitely check the following page:

https://github.com/Netflix/Hystrix/wiki/How-it-Works

cheers

Tomas