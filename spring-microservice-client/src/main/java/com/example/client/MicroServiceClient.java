package com.example.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class MicroServiceClient implements CommandLineRunner {

	@Autowired
	protected MicroServiceInvoker microServiceInvoker;
    

	@Override
	public void run(String... arg0) throws Exception {
		long i = 0;
		while(true) {
			System.out.println("Invocation number :"+i);
			microServiceInvoker.invokeMicroService();
			Thread.sleep(200);
			i++;
		}
	}
}
