package com.example.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
public class PersonsController {
		
    @RequestMapping("/persons")
    public Persons getPersonById() {
    	final Persons persons = new Persons();
    	final Person person = new Person();
    	person.setName("Tomas");
    	person.setSurname("Kloucek");
    	person.setDepartment("Programmer");
    	persons.getPersons().add(person);
    	
    	return persons;
    }
}
